<!DOCTYPE html>
<html>
<head>
	<title>crud</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<a href="menu4.html">Back</a>
<?php 
	include "koneksi.php";
	//MENAMBAH DATA
	if (isset($_POST['upload'])) {
		$title=$_POST['title'];
		$kategori=$_POST['kategori'];
		$name=$_FILES['attache']['name'];
    	$type=$_FILES['attache']['type'];
   		$size=$_FILES['attache']['size'];
   		$nama_file=str_replace(" ","_",$name);
    	$tmp_name=$_FILES['attache']['tmp_name'];
    	$nama_folder="video/";
    	$file_baru=$nama_folder.basename($nama_file);
    	if ((($type == "video/mp4") || ($type == "video/3gpp")) && ($size < 800000000)){
       		move_uploaded_file($tmp_name,$file_baru);
       		echo "Upload file attache $nama_file berhasil diupload";

       		$name2=$_FILES['thumbnail']['name'];
    		$type2=$_FILES['thumbnail']['type'];
   			$size2=$_FILES['thumbnail']['size'];
   			$nama_file2=str_replace(" ","_",$name2);
    		$tmp_name2=$_FILES['thumbnail']['tmp_name'];
    		$nama_folder2="thumbnail/";
    		$file_baru2=$nama_folder2.basename($nama_file2);
    			if ((($type == "video/mp4") || ($type == "video/3gpp")) && ($size < 800000000 )){
       				move_uploaded_file($tmp_name2,$file_baru2);
       				echo "Upload file thumbnail $nama_file berhasil diupload";
       				$sql2="INSERT INTO video_tb(title,category_id,attache,thumbnail) VALUES('$title','$kategori','$name','$name2')";
       				$query2=mysqli_query($koneksi,$sql2);
       				header("Location: crud42.php");
    			}
    		else{
        		echo "File thumbnail Terlalu Besar Atau Format Video Salah!";
    		}
    	}
    	else{
        	echo "File attache Terlalu Besar Atau Format Video Salah!";
    	}

    
	}
?> 
<?php 
	//aksi
	if (isset($_GET['aksi'])) {
		$aksi=$_GET['aksi'];
		if ($aksi=='delete') {
			$idd=$_GET['id'];
			$nama_file=$_GET['nf'];
			$nama_file_t=$_GET['nft'];
			$sql3="DELETE FROM video_tb where id='$idd'";
			$query3=mysqli_query($koneksi,$sql3);
			$file=("video/$nama_file");
			$file_t=("thumbnail/$nama_file_t");
			if (file_exists($file)) {
			if (unlink($file)) {
				echo "Success";
					if (unlink($file_t)) {
						echo "Success delete thumbnail";
					}
				}else{ 
					echo "Fail";
				}
			}else{
			echo "File Doesn't exist";
		}
			header("Location: crud42.php");
		}elseif ($aksi=='ubah') {
			$idu=$_GET['id'];
			$sql4="SELECT*FROM video_tb where id='$idu'";
			$query4=mysqli_query($koneksi,$sql4);
			$data4=mysqli_fetch_array($query4);
			echo "<form enctype='multipart/form-data'>";
			echo "<label class='mb-2 mr-sm-2'>ID</label>";
			echo "<input type='text' class='form-control mb-2 mr-sm-2' name='idubah' value='".$data4['id']."' readonly>";
			echo "<label class='mb-2 mr-sm-2'>Title</label>";
			echo "<input type='text' class='form-control mb-2 mr-sm-2' name='titleubah' value='".$data4['title']."'>";
			echo "<label>Kategori</label>";
			echo "<select name='kategori_u' class='form-control mb-2 mr-sm-2'>";
			$sql6="SELECT*FROM category_tb";
			$query6=mysqli_query($koneksi,$sql6);
				while ($data6=mysqli_fetch_array($query6)) {
					echo "<option value='". $data6['id'] ."'>". $data6['name'] ."</option>";
				}
			echo "</select><br>";
			echo "<input type='submit' name='ubahkirim' value='Ubah' class='btn btn-primary mb-2'>";
			echo "</form>";
		}
	}
	if (isset($_GET['idubah'])) {
			$idu2 = $_GET['idubah'];
			$titleubah2 = $_GET['titleubah'];
			$kategori_u= $_GET['kategori_u'];
			$sql5 = "UPDATE video_tb SET title='".$titleubah2."',category_id='".$kategori_u."' WHERE id='".$idu2."'";
			$query5 = mysqli_query($koneksi,$sql5);
			header("Location: crud42.php");
		}
?>
<form enctype="multipart/form-data" method="POST">
	<label class='mb-2 mr-sm-2'>Tambah Data</label>
	<input type="text" name="title" class='form-control mb-2 mr-sm-2' placeholder="Title">
	<label>Kategori</label>
	<select name="kategori" class='form-control mb-2 mr-sm-2'>
		<?php
		$sql6="SELECT*FROM category_tb";
		$query6=mysqli_query($koneksi,$sql6);
		while ($data6=mysqli_fetch_array($query6)) {
		?>
		<option value="<?= $data6['id'] ?>"><?= $data6['name'] ?></option>
		<?php
		}
		?>
	</select><br>
	<label>Attache</label>
	<input type="file" name="attache" class='form-control mb-2 mr-sm-2' placeholder="Attache">
	<label>Thumbnail</label>
	<input type="file" name="thumbnail" class='form-control mb-2 mr-sm-2' placeholder="Thumbnail">
	<input type="submit" name="upload" value="Tambah" class='btn btn-primary mb-2'>
</form>
	<?php
	//menampilkan data
			$sql = "SELECT*FROM video_tb";
			$query = mysqli_query($koneksi,$sql);

	?>
	<h2>Data Category</h2>
	<table border="1" class="table">
		<thead class="thead-dark">
		<tr>
			<th>ID</th> 
			<th>Title</th>
			<th>Kategori</th>
			<th>Attache</th>
			<th>Thumbnail</th>
			<th>Action</th>
		</tr>
		</thead>
	<?php
		while ($data=mysqli_fetch_array($query)) {
			
		
	?>
		<tr>
			<td><?=  $data['id']?></td>
			<td><?=  $data['title']?></td>
			<td><?=  $data['category_id']?></td>
			<td><?=  $data['attache']?></td>
			<td><?=  $data['thumbnail']?></td>
			<td><a href="crud42.php?aksi=delete&id=<?= $data['id'] ?>&nf=<?= $data['attache'] ?>&nft=<?= $data['thumbnail'] ?>">Delete</a> |
				<a href="crud42.php?aksi=ubah&id=<?= $data['id'] ?>">Edit</a></td>
		</tr>
	<?php
		}
	?>
	</table>
</div>
</body>
</html>